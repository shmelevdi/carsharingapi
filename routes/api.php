<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('auth', 'ApiCarsharing@index');
Route::get('auth/{id}', 'ApiCarsharing@show');
Route::post('auth', 'ApiCarsharing@store');
Route::put('auth/{id}', 'ApiCarsharing@update');
Route::delete('auth/{id}', 'ApiCarsharing@delete');

Route::get('/users', function () {
    return \App\User::all();
});