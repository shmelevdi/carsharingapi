<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded = [];

    public function model()
    {
        return $this->belongsTo('App\CarModel', 'model_id');
    }

    public function color()
    {
        return $this->belongsTo('App\Color', 'color_id');
    }

    public function car_type()
    {
        return $this->belongsTo('App\CarCat', 'carcat_id');
    }

    public function fuel_type()
    {
        if ($this->fuel_type == 1) return 'АИ-92';
        elseif ($this->fuel_type == 2) return 'АИ-95';
        elseif ($this->fuel_type == 3) return 'АИ-98';
        return 'АИ-100, 101, 102';
    }

    public function transmission()
    {
        if ($this->gearbox_type == 1) return 'Механическая коробка';
        elseif ($this->gearbox_type == 2) return 'Автоматическая коробка';
        elseif ($this->gearbox_type == 3) return 'Роботизированная коробка';
        return 'Вариативная коробка';
    }

}
