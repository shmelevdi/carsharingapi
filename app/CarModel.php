<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    protected $table = 'car_models';
    protected $guarded = [];

    public function manufacturer()
    {
        return $this->belongsTo('App\Manufacturer', 'id_manufacturer');
    }
}
