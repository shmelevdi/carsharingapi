<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarCat extends Model
{
    protected $table = 'carcat';
    protected $guarded = [];
}
