<?php

namespace App\Http\Controllers;

use App\Car;
use App\CarCat;
use App\CarModel;
use App\Color;
use Illuminate\Http\Request;

class CarsController extends Controller
{
    public function index()
    {
        $cars = \App\Car::all();
        return view('cars.index', compact('cars'));
    }

    public function create()
    {
        $carModels = CarModel::with('manufacturer')->get();
        $colors = Color::all();
        $carcat = CarCat::all();
        $transmissions = array(
            ['id' => 1, 'name' => 'Механическая коробка'],
            ['id' => 2, 'name' => 'Автоматическая коробка'],
            ['id' => 3, 'name' => 'Роботизированная коробка'],
            ['id' => 4, 'name' => 'Вариативная коробка'],
        );
        $transmissions = json_decode(json_encode($transmissions));

        $fuelTypes = array(
            ['id' => 1, 'name' => 'АИ-92'],
            ['id' => 2, 'name' => 'АИ-95'],
            ['id' => 3, 'name' => 'АИ-98'],
            ['id' => 4, 'name' => 'АИ-100, 101, 102'],
        );
        $fuelTypes = json_decode(json_encode($fuelTypes));

        return view('cars.create', compact('carModels', 'colors', 'carcat', 'transmissions', 'fuelTypes'));
    }

    public function store(Request $request)
    {
        \App\Car::create([
            'model_id' => $request->model,
            'gov_number' => $request->number,
            'color_id' => $request->color,
            'gearbox_type' => $request->transmission,
            'fuel_type' => $request->fuel_type,
            'fuel_cons' => $request->consumption,
            'carcat_id' => $request->carcat,
        ]);
        return redirect(route('cars.index'));
    }

    public function edit(Car $car)
    {
        $carModels = CarModel::with('manufacturer')->get();
        $colors = Color::all();
        $carcat = CarCat::all();
        $transmissions = array(
            ['id' => 1, 'name' => 'Механическая коробка'],
            ['id' => 2, 'name' => 'Автоматическая коробка'],
            ['id' => 3, 'name' => 'Роботизированная коробка'],
            ['id' => 4, 'name' => 'Вариативная коробка'],
        );
        $transmissions = json_decode(json_encode($transmissions));

        $fuelTypes = array(
            ['id' => 1, 'name' => 'АИ-92'],
            ['id' => 2, 'name' => 'АИ-95'],
            ['id' => 3, 'name' => 'АИ-98'],
            ['id' => 4, 'name' => 'АИ-100, 101, 102'],
        );
        $fuelTypes = json_decode(json_encode($fuelTypes));

        return view('cars.edit', compact('car', 'carModels', 'colors', 'carcat', 'transmissions', 'fuelTypes'));
    }

    public function update(Request $request, Car $car)
    {
        $car->update([
            'model_id' => $request->model,
            'gov_number' => $request->number,
            'color_id' => $request->color,
            'gearbox_type' => $request->transmission,
            'fuel_type' => $request->fuel_type,
            'fuel_cons' => $request->consumption,
            'carcat_id' => $request->carcat,
        ]);
        return redirect(route('cars.index'));
    }

    public function show()
    {

    }

    public function destroy(Car $car)
    {
        $car->delete();
        return redirect(route('cars.index'));
    }
}
