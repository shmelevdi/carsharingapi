<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('car_documents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('pts')->nullable();
			$table->string('osago')->nullable();
			$table->string('kasko')->nullable();
			$table->string('car_cert')->nullable();
			$table->string('instruction')->nullable();
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('car_documents');
	}

}
