<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassportScansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passport_scans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('serial');
            $table->unsignedBigInteger('number');
            $table->timestamps();
//            $table->unique(['serial, number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passport_scans');
    }
}
