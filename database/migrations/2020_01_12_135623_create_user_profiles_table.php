<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedBigInteger('user_id')->unique();
            $table->foreign('user_id')->on('users')->references('id');

            $table->unsignedBigInteger('passport_id')->nullable();
            $table->foreign('passport_id')->on('passport_scans')->references('id');

            $table->unsignedBigInteger('driver_card_id')->nullable();
            $table->foreign('driver_card_id')->on('driver_cards')->references('id');

            $table->string('fam')->nullable();
            $table->string('nam')->nullable();
            $table->string('otch')->nullable();
            $table->date('birthday')->nullable();

            $table->string('address')->nullable();
            $table->string('city')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
