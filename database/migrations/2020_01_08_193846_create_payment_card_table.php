<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentCardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_card', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
            $table->foreign('user_id')->on('users')->references('id');

            $table->bigInteger('card_num')->nullable();
			$table->integer('validity')->nullable();
			$table->integer('cvc2')->nullable();
			$table->string('holder')->nullable();
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_card');
	}

}
