<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarModelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('car_models', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_manufacturer')->nullable();
            $table->foreign('id_manufacturer')->on('manufacturers')->references('id');
            $table->string('model_name')->nullable();
			$table->integer('manufacturer_year')->nullable();
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('car_models');
	}

}
