<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->default('1');
            $table->foreign('role_id')->on('roles')->references('id');

            $table->string('username')->unique();
            $table->string('password');
            $table->string('phone')->unique()->nullable(); // 9001234567
            $table->string('email')->unique();

            $table->rememberToken();

            $table->dateTime('last_login_date')->nullable();
            $table->string('last_login_ip', 64)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
