<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTripsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trips', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->integer('user_id');
            $table->foreign('user_id')->on('users')->references('id');

            $table->integer('car_id')->nullable();
            $table->foreign('car_id')->on('cars')->references('id');

            $table->dateTime('trip_start')->nullable();
			$table->dateTime('trip_end')->nullable();

			$table->integer('id_cashreciept')->nullable();
			$table->integer('price_reserv')->nullable();
			$table->integer('price_general')->nullable();
			$table->integer('price_parking')->nullable();
			$table->integer('price_insurance')->nullable();
			$table->integer('price_ontrip')->nullable();
			$table->integer('price_viewcar')->nullable();

			$table->time('time_reserv')->nullable();
			$table->time('time_general')->nullable();
			$table->time('time_parking')->nullable();
			$table->time('time_ontrip')->nullable();
			$table->integer('status')->nullable();
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trips');
	}

}
