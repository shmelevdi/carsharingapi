<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('serial');
            $table->unsignedBigInteger('number');

            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('driver_card_categories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_cards');
    }
}
