<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cars', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('model_id')->nullable();
            $table->foreign('model_id')->on('car_models')->references('id');

            $table->string('gov_number', 32)->nullable();
			$table->integer('color_id')->nullable();
            $table->foreign('color_id')->on('colors')->references('id');

            $table->integer('gearbox_type')->nullable();

			$table->integer('document_id')->nullable();
            $table->foreign('document_id')->on('car_documents')->references('id');

            $table->string('fuel_cons', 8)->nullable();
			$table->integer('fuel_type')->nullable();
			$table->integer('fuel_count')->nullable();

			$table->integer('carcat_id')->nullable();
            $table->foreign('carcat_id')->on('carcat')->references('id');

            $table->integer('status')->nullable();
			$table->boolean('isopen')->nullable();
			$table->boolean('is_startengine')->nullable();
			$table->boolean('isopen_window')->nullable();
			$table->boolean('ison_lights')->nullable();
			$table->string('gps_coords', 256)->nullable();
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cars');
	}

}
