<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarcatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carcat', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('carcat_name')->nullable();
			$table->integer('price_km')->nullable();
			$table->integer('price_wait')->nullable();
			$table->time('free_wait_start')->nullable();
			$table->time('free_wait_end')->nullable();
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carcat');
	}

}
