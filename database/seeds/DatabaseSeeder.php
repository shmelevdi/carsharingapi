<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
//        DB::table('roles')->insert(['role_name' => 'Пользователь']);
//        DB::table('roles')->insert(['role_name' => 'Администратор']);
//
//        DB::table('users')->insert(['username' => 'admin', 'role_id' => '2', 'password' => bcrypt('123'), 'email' => 'admin@admin.com']);
//        DB::table('users')->insert(['username' => 'user_1', 'password' => bcrypt('123'), 'email' => 'user1@test.com', 'phone' => '9041230012']);
//        DB::table('users')->insert(['username' => 'user_2', 'password' => bcrypt('123'), 'email' => 'user2@test.com', 'phone' => '9031230012']);
//
//        DB::table('passport_scans')->insert(['serial' => '4217', 'number' => '123456']);
//        DB::table('passport_scans')->insert(['serial' => '4218', 'number' => '234567']);
//        DB::table('passport_scans')->insert(['serial' => '4219', 'number' => '345678']);
//
//        DB::table('driver_card_categories')->insert(['category_type' => 'A']);
//        DB::table('driver_card_categories')->insert(['category_type' => 'A1']);
//        DB::table('driver_card_categories')->insert(['category_type' => 'B']);
//        DB::table('driver_card_categories')->insert(['category_type' => 'B1']);
//        DB::table('driver_card_categories')->insert(['category_type' => 'C']);
//        DB::table('driver_card_categories')->insert(['category_type' => 'C1']);
//
//        DB::table('driver_cards')->insert(['serial' => '4817', 'number' => '123456', 'category_id' => 1]);
//        DB::table('driver_cards')->insert(['serial' => '4818', 'number' => '234567', 'category_id' => 2]);
//        DB::table('driver_cards')->insert(['serial' => '4819', 'number' => '345678', 'category_id' => 3]);
//
//        DB::table('user_profiles')->insert(['user_id' => 1, 'passport_id' => 1, 'driver_card_id' => 1, 'fam' => 'Иванов', 'nam' => 'Иван', 'address' => 'Советская, 66', 'city' => 'Липецк']);
//        DB::table('user_profiles')->insert(['user_id' => 2, 'passport_id' => 2, 'driver_card_id' => 2, 'fam' => 'Петров', 'nam' => 'Владимир', 'address' => 'Московская, 45', 'city' => 'Липецк']);
//        DB::table('user_profiles')->insert(['user_id' => 3, 'passport_id' => 3, 'driver_card_id' => 3, 'fam' => 'Зайцев', 'nam' => 'Вячеслав', 'address' => 'Космонавтов, 99', 'city' => 'Липецк']);



        ///
        ///
        DB::table('manufacturers')->insert(['manufacturer_name' => 'Toyota']);
        DB::table('manufacturers')->insert(['manufacturer_name' => 'Hyundai']);
        DB::table('manufacturers')->insert(['manufacturer_name' => 'Kia']);
        DB::table('manufacturers')->insert(['manufacturer_name' => 'Ford']);
        DB::table('manufacturers')->insert(['manufacturer_name' => 'Chevrolet']);

        DB::table('car_models')->insert(['id_manufacturer' => '1', 'model_name' => 'Corolla', 'manufacturer_year' => '2015']);
        DB::table('car_models')->insert(['id_manufacturer' => '1', 'model_name' => 'Camry', 'manufacturer_year' => '2012']);
        DB::table('car_models')->insert(['id_manufacturer' => '1', 'model_name' => 'Supra', 'manufacturer_year' => '2017']);
        DB::table('car_models')->insert(['id_manufacturer' => '2', 'model_name' => 'Solaris', 'manufacturer_year' => '2014']);
        DB::table('car_models')->insert(['id_manufacturer' => '2', 'model_name' => 'Creta', 'manufacturer_year' => '2016']);
        DB::table('car_models')->insert(['id_manufacturer' => '2', 'model_name' => 'Sonata', 'manufacturer_year' => '2013']);
        DB::table('car_models')->insert(['id_manufacturer' => '3', 'model_name' => 'Rio', 'manufacturer_year' => '2018']);
        DB::table('car_models')->insert(['id_manufacturer' => '3', 'model_name' => 'Soul', 'manufacturer_year' => '2013']);
        DB::table('car_models')->insert(['id_manufacturer' => '3', 'model_name' => 'Cerato', 'manufacturer_year' => '2014']);
        DB::table('car_models')->insert(['id_manufacturer' => '4', 'model_name' => 'Focus', 'manufacturer_year' => '2015']);
        DB::table('car_models')->insert(['id_manufacturer' => '4', 'model_name' => 'Fiesto', 'manufacturer_year' => '2011']);
        DB::table('car_models')->insert(['id_manufacturer' => '4', 'model_name' => 'Fiesto', 'manufacturer_year' => '2013']);
        DB::table('car_models')->insert(['id_manufacturer' => '5', 'model_name' => 'Malibu', 'manufacturer_year' => '2012']);
        DB::table('car_models')->insert(['id_manufacturer' => '5', 'model_name' => 'Spark', 'manufacturer_year' => '2018']);
        DB::table('car_models')->insert(['id_manufacturer' => '5', 'model_name' => 'Silverado', 'manufacturer_year' => '2014']);

        DB::table('colors')->insert(['color_name' => 'Красный']);
        DB::table('colors')->insert(['color_name' => 'Синий']);
        DB::table('colors')->insert(['color_name' => 'Жёлтый']);
        DB::table('colors')->insert(['color_name' => 'Зелёный']);
        DB::table('colors')->insert(['color_name' => 'Серебристый']);
        DB::table('colors')->insert(['color_name' => 'Белый']);
        DB::table('colors')->insert(['color_name' => 'Чёрный']);

        DB::table('carcat')->insert(['carcat_name' => 'Эконом', 'price_km' => 5, 'price_wait' => 2]);
        DB::table('carcat')->insert(['carcat_name' => 'Комфорт', 'price_km' => 8, 'price_wait' => 3]);
        DB::table('carcat')->insert(['carcat_name' => 'Люкс', 'price_km' => 11, 'price_wait' => 4]);


    }
}
