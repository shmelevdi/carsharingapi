@extends('layouts.base')

@section('content')
    @include('menu')
    <div class="content">
        <h1>Администратор</h1>

        <div class="tabs">
            <div class="tab-item" onclick="openCity('users')">Пользователи</div>
            <div class="tab-item" onclick="openCity('car-wash')">Автомойки</div>
            <div class="tab-item" onclick="openCity('trips')">Поездки</div>
            <div class="tab-item" onclick="openCity('settings')">Настройки</div>
        </div>

        <div id="users" class="tab-content">
            <table class="table">
                <thead>
                <th>ID</th>
                <th>ФИО</th>
                <th>Логин</th>
                <th>Статус</th>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Шмелев Дмитрий Е</td>
                    <td>admin</td>
                    <td class="active">Активен</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Федерякина Полина С</td>
                    <td>admin</td>
                    <td class="active">Активен</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Шацких Даниил А</td>
                    <td>admin</td>
                    <td class="active">Активен</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Шмелев Дмитрий Е</td>
                    <td>admin</td>
                    <td>Активен</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id="car-wash" class="tab-content" style="display:none">
            <h1>Автомойки</h1>
        </div>

        <div id="trips" class="tab-content" style="display:none">
            <h1>Поездки</h1>
        </div>

        <div id="settings" class="tab-content" style="display:none">
            <h1>Настройки</h1>
        </div>
    </div>

    <script>
        function openCity(tabContent) {
            var i;
            var x = document.getElementsByClassName("tab-content");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById(tabContent).style.display = "block";
        }
    </script>
@endsection