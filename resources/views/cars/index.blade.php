@extends('layouts.base')

@section('content')
    @include('menu')
    <div class="content">
        <h1>Список автомобилей</h1>
        <table class="table">
            <thead>
                <th>Модель</th>
                <th>Номер</th>
                <th>Цвет</th>
                <th>Тип КПП</th>
                <th>Тип топлива</th>
                <th>Потребление топлива</th>
                <th>Класс машины</th>
            </thead>
            <tbody>
            @foreach($cars as $car)
                <tr>
                    <td>{{ $car->model->manufacturer->manufacturer_name }} {{ $car->model->model_name }}</td>
                    <td>{{ $car->gov_number }}</td>
                    <td>{{ $car->color->color_name }}</td>
                    <td>{{ $car->transmission() }}</td>
                    <td>{{ $car->fuel_type() }}</td>
                    <td>{{ $car->fuel_cons }}</td>
                    <td>{{ $car->car_type['carcat_name'] }}</td>
                    <td>
                        <a href="{{ route('cars.edit', $car) }}">Редактировать</a>
                    </td>
                    <td>
                        <form action="{{ route('cars.destroy', $car) }}" method="POST">
                            <input name="_car_id" type="hidden"  value="{{$car->id}}">
                            <button type="submit">Удалить</button>
                            {!! method_field('delete') !!}
                            {!! csrf_field() !!}
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection