@extends('layouts.base')

@section('content')
    @include('menu')
    <div class="content">
        <h1>Добавление автомобиля</h1>
        <form action="{{ route('cars.store') }}" method="post" class="authform">
            {{ csrf_field() }}

            <label for="model">Модель</label>
            <select name="model" id="model" class="form-control">
                @foreach($carModels as $carModel)
                    <option value="{{ $carModel->id }}">{{ $carModel->manufacturer->manufacturer_name  }} {{ $carModel->model_name }}</option>
                @endforeach
            </select>
            <input name="number" type="text" placeholder="Номер автомобиля">

            <label for="color">Цвет</label>
            <select name="color" id="color" class="form-control">
                @foreach($colors as $color)
                    <option value="{{ $color->id }}">{{ $color->color_name }}</option>
                @endforeach
            </select>

            <label for="transmission">Тип КПП</label>
            <select name="transmission" id="transmission" class="form-control">
                @foreach($transmissions as $transmission)
                    <option value="{{ $transmission->id }}">{{ $transmission->name }}</option>
                @endforeach
            </select>

            <label for="fuel_type">Тип топлива</label>
            <select name="fuel_type" id="fuel_type" class="form-control">
                @foreach($fuelTypes as $fuelType)
                    <option value="{{ $fuelType->id }}">{{ $fuelType->name }}</option>
                @endforeach
            </select>

            <input name="consumption" type="text" placeholder="Потребление топлива">

            <label for="carcat">Классы машины</label>
            <select name="carcat" id="carcat" class="form-control">
                @foreach($carcat as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->carcat_name }}</option>
                @endforeach
            </select>
            <button type="submit">Добавить</button>
        </form>
    </div>
@endsection