@extends('layouts.base')

@section('content')
    @include('menu')
    <div class="content">
        <h1>Редактирование автомобиля</h1>
        <form action="{{ route('cars.update', $car) }}" method="post" class="authform">
            {!! method_field('patch') !!}
            {!! csrf_field() !!}

            <div>
                <label for="model">Модель</label>
                <select name="model" id="model" class="form-control">
                    @foreach($carModels as $carModel)
                        @if($carModel->id == $car->model_id)
                            <option selected
                                    value="{{ $carModel->id }}">{{ $carModel->manufacturer->manufacturer_name  }} {{ $carModel->model_name }}</option>
                        @else
                            <option value="{{ $carModel->id }}">{{ $carModel->manufacturer->manufacturer_name  }} {{ $carModel->model_name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div>
                <input name="number" type="text" placeholder="Номер автомобиля" value="{{ $car->gov_number }}">
            </div>

            <div>
                <label for="color">Цвет</label>
                <select name="color" id="color" class="form-control">
                    @foreach($colors as $color)
                        @if($color->id == $car->color_id)
                            <option selected value="{{ $color->id }}">{{ $color->color_name }}</option>
                        @else
                            <option value="{{ $color->id }}">{{ $color->color_name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div>
                <label for="transmission">Тип КПП</label>
                <select name="transmission" id="transmission" class="form-control">
                    @foreach($transmissions as $transmission)
                        @if($transmission->id == $car->gearbox_type)
                            <option selected value="{{ $transmission->id }}">{{ $transmission->name }}</option>
                        @else
                            <option value="{{ $transmission->id }}">{{ $transmission->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div>
                <label for="fuel_type">Тип топлива</label>
                <select name="fuel_type" id="fuel_type" class="form-control">
                    @foreach($fuelTypes as $fuelType)
                        @if($fuelType->id == $car->fuel_type)
                            <option selected value="{{ $fuelType->id }}">{{ $fuelType->name }}</option>
                        @else
                            <option value="{{ $fuelType->id }}">{{ $fuelType->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div>
                <input name="consumption" type="text" placeholder="Потребление топлива" value="{{ $car->fuel_cons }}">
            </div>

            <div>
                <label for="carcat">Классы машины</label>
                <select name="carcat" id="carcat" class="form-control">
                    @foreach($carcat as $cat)
                        @if($cat->id == $car->carcat_id)
                            <option selected value="{{ $cat->id }}">{{ $cat->carcat_name }}</option>
                        @else
                            <option value="{{ $cat->id }}">{{ $cat->carcat_name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <button type="submit">Сохранить</button>
        </form>
    </div>
@endsection