<div class="menu">
    <div class="menu-profile">
        <div class="menu-profile-name">Дмитрий Шмелёв</div>
        <div class="menu-logout">Выход</div>
    </div>
    <a class="menu-item profile" href="{{ route('register') }}">Регистрация</a>
    <a class="menu-item profile" href="{{ route('login') }}">Вход</a>
    <a class="menu-item profile" href="/home">Мой профиль</a>
    <a class="menu-item car" href="/map">Арендовать авто</a>
    <a class="menu-item profile" href="/admin">Администратор</a>
    <a class="menu-item car" href="{{ route('cars.create') }}">Добавить авто</a>
    <a class="menu-item car" href="{{ route('cars.index') }}">Список авто</a>
</div>