<!DOCTYPE html>
<html lang="ru" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Авторизация - Carsharing</title>
    <script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/jquery.maskedinput.min.js"></script>
    <script src="../js/masked.js"></script>
    <link rel="stylesheet" href="../css/auth_style.css">
</head>
<body>

<div class="authbg">
    <div class="authwrap">
        <img src="../img/logo.png" class="logo">
        <form action="{{ route('login') }}" method="POST" class="authform">
            {{ csrf_field() }}
            <input name="username" type="text" class="inlogin" placeholder="Логин">
            <input name="password" type="password" class="inpassword" placeholder="Пароль">

            <div class="submitwrap">
                <a href="{{ route('register') }}">Регистрация</a>
                <button type="submit" class="login">
                    Войти
                </button>
            </div>

        </form>
    </div>
</div>
</body>
</html>
