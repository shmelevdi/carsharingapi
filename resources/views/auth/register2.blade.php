<!DOCTYPE html>
<html lang="ru" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Авторизация - Carsharing</title>
    <script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/jquery.maskedinput.min.js"></script>
    <script src="../js/masked.js"></script>
    <link rel="stylesheet" href="../css/auth_style.css">
</head>
<body>


<div class="authbg">
    <div class="authwrap">
        <img src="../img/logo.png" class="logo">
        <div class="registerblock">
            <img class="backbut" src="../img/back.png">
            <div class="regtitle">Регистрация</div>
            <img src="../img/next.png">
        </div>
        <div class="regval">
            <div class="regvalue"></div>
        </div>
        <form class="regform" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <input id="username" type="text" name="username" value="{{ old('name') }}" class="inlogin"
                   placeholder="Логин" required autofocus>
            <input id="tel" type="tel" name="phone" value="{{ old('phone') }}" class="inphone"
                   placeholder="+7 (903) 000 00 00" required autofocus>
            <input id="email" type="email" name="email" value="{{ old('email') }}" class="inemail" placeholder="Email"
                   required>
            <input id="password" type="password" name="password" class="inpassword" placeholder="Пароль" required>
            <input id="password-confirm" type="password" name="password_confirmation" class="inpasswordtwo"
                   placeholder="Повтор пароля" required>

            <div class="submitwrap">
                <button type="submit" class="nextreg">
                    Зарегистрироваться
                </button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
